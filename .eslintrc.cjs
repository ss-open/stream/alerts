module.exports = {
  root: true,
  env: {
    browser: true,
    es2020: true,
  },
  globals: {
    "$": "readonly",
    "setRefresh": "readonly"
  },
  extends: ['eslint:recommended'],
};

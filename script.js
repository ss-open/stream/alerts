var videoElement = document.createElement('video');
videoElement.src = '';
videoElement.volume = 1;
videoElement.autoplay = 1;
videoElement.style = 'position: absolute; bottom: 0; right: 0; width: 100%'
videoElement.onended = function() {
  $('.screen_div').fadeOut('slow', function() {
    document.querySelector('.screen_div').remove();
    setRefresh();
  });
};
document.querySelector('.screen_div').append(videoElement);
